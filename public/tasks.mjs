const taskElement = (template, task) => {
  const element = template.content.cloneNode(true);
  element.querySelector('p.name').innerText = task.title;
  return element;
};
const createdTask = () => {
  const title = new URL(document.location).searchParams.get('task');
  return title ? { title } : null;
};
const storedTasks = () => [];  // TODO
const allTasks = () => [createdTask(), ...storedTasks()].filter(Boolean);
allTasks()
  .map(t => taskElement(document.querySelector('template.task'), t))
  .forEach(t => document.querySelector('menu.tasks').appendChild(t));
